rootProject.name = "Brewery"
include(
    ":app",
    ":domain",
    ":data",
    ":common"
)
