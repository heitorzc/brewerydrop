buildscript {
    Config.Repositories.setupRepositories(repositories)

    dependencies {
        classpath(Config.BuildPlugins.androidGradle)
        classpath(Config.BuildPlugins.kotlinGradlePlugin)
        classpath(Config.BuildPlugins.navigationSafeArgsGradlePlugin)
    }
}

allprojects {
    Config.Repositories.setupRepositories(repositories)
}

tasks {
    val clean by registering(Delete::class) {
        delete(buildDir)
    }
}

