import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
    id(Config.Plugins.androidApplication)
    kotlin(Config.Plugins.kotlinAndroid)
    kotlin(Config.Plugins.kotlinAndroidExtensions)
    kotlin(Config.Plugins.kotlinKapt)
    id(Config.Plugins.navigationSafeArgs)
    id("org.sonarqube").version("2.8")
}

sonarqube {
    properties {
        property("sonar.projectKey", "heitorzc_brewerydrop")
        property("sonar.organization", "heitorzc")
        property("sonar.host.url", "https://sonarcloud.io")
        property("sonar.login", "459dbf16251f08f3ab2a8425851d6c506365c1ef")
    }
}

android {
    compileSdkVersion(Config.Android.compileSdkVersion)
    buildToolsVersion(Config.Android.buildToolsVersion)

    defaultConfig {
        minSdkVersion(Config.Android.minSdkVersion)
        targetSdkVersion(Config.Android.targetSdkVersion)
        applicationId = Config.Android.applicationId
        versionCode = Config.Android.versionCode
        versionName = Config.Android.versionName

        testInstrumentationRunner = Config.Android.testInstrumentationRunner
    }

    buildTypes {
        getByName("release") {
            isMinifyEnabled = true
            proguardFiles(getDefaultProguardFile("proguard-android-optimize.txt"), "proguard-rules.pro")
        }
    }

    compileOptions {
        sourceCompatibility = JavaVersion.VERSION_1_3
        targetCompatibility = JavaVersion.VERSION_1_3
    }

    dataBinding {
        isEnabled = true
    }
}

// compile bytecode to java 8 (default is java 6)
tasks.withType<KotlinCompile> {
    kotlinOptions.jvmTarget = "13"
}

dependencies {
    implementation(project(":common"))
    implementation(project(":domain"))

    implementation(Config.Libs.appcompat)
    implementation(Config.Libs.material)
    implementation(Config.Libs.lifecycleViewModel)
    implementation(Config.Libs.navigationFragment)
    implementation(Config.Libs.navigationUi)

    implementation(Config.Libs.coil)

    testImplementation(Config.TestLibs.androidxTestCore)
    testImplementation(Config.TestLibs.archCoreTest)
    testImplementation(Config.TestLibs.junitTest)
    testImplementation(Config.TestLibs.roboletric)
    testImplementation(Config.TestLibs.koinTest)
    testImplementation(Config.TestLibs.mockK)
    testImplementation(Config.TestLibs.coroutinesTest)

    androidTestImplementation(Config.TestLibs.junitAndroidTest)
    androidTestImplementation(Config.TestLibs.espressoCore)
}
