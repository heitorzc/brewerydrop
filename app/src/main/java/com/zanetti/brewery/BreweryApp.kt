package com.zanetti.brewery

import android.app.Application
import com.zanetti.brewery.core.di.FeatureModule
import com.zanetti.brewery.domain.di.DomainModule
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin

class BreweryApp: Application() {
    override fun onCreate() {
        super.onCreate()

        startKoin {
            androidContext(this@BreweryApp)
            modules(listOf(
                DomainModule.get(),
                FeatureModule.get()
            ))
        }

    }
}