package com.zanetti.brewery.feature.beerdetails.adapter.hop

import com.zanetti.brewery.core.base.adapter.ViewEntityBindingViewHolder
import com.zanetti.brewery.databinding.AdapterHopsListItemBinding

class HopViewHolder(private val binding: AdapterHopsListItemBinding): ViewEntityBindingViewHolder<HopViewEntity>(binding.root) {
    override fun bind(item: HopViewEntity) {
        binding.viewEntity = item
        binding.executePendingBindings()
    }
}