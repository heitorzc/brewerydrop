package com.zanetti.brewery.feature.beerdetails

import android.app.Application
import androidx.lifecycle.*
import com.zanetti.brewery.common.core.onFailure
import com.zanetti.brewery.common.core.onSuccess
import com.zanetti.brewery.common.model.Beer
import com.zanetti.brewery.common.model.apiraw.HopsItemRAW
import com.zanetti.brewery.common.model.apiraw.MaltItemRAW
import com.zanetti.brewery.common.model.apiraw.MethodRAW
import com.zanetti.brewery.core.base.SingleLiveEvent
import com.zanetti.brewery.domain.usecase.GetBeerByIdUseCase
import com.zanetti.brewery.feature.beerdetails.adapter.hop.HopViewEntity
import com.zanetti.brewery.feature.beerdetails.adapter.hop.HopViewEntityMapper
import com.zanetti.brewery.feature.beerdetails.adapter.malt.MaltViewEntity
import com.zanetti.brewery.feature.beerdetails.adapter.malt.MaltViewEntityMapper
import com.zanetti.brewery.feature.beerdetails.adapter.method.MethodViewEntity
import com.zanetti.brewery.feature.beerdetails.adapter.method.MethodViewEntityMapper
import com.zanetti.brewery.feature.beerdetails.viewentity.BeerDetailsViewEntity
import com.zanetti.brewery.feature.beerdetails.viewentity.BeerDetailsViewEntityMapper
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class BeerDetailsViewModel(application: Application,
                           beerId: Int?,
                           private val viewEntityMapper: BeerDetailsViewEntityMapper,
                           private val hopViewEntityMapper: HopViewEntityMapper,
                           private val maltViewEntityMapper: MaltViewEntityMapper,
                           private val methodViewEntityMapper: MethodViewEntityMapper,
                           private val getBeerByIdUseCase: GetBeerByIdUseCase): AndroidViewModel(application) {

    private val _populateBeerDetails = MediatorLiveData<BeerDetailsViewEntity>()
    val populateBeerDetails: LiveData<BeerDetailsViewEntity> = _populateBeerDetails

    private val _hopsList = MediatorLiveData<List<HopViewEntity>>()
    val hopsList: LiveData<List<HopViewEntity>> = _hopsList

    private val _maltList = MediatorLiveData<List<MaltViewEntity>>()
    val maltList: LiveData<List<MaltViewEntity>> = _maltList

    private val _methodList = MediatorLiveData<List<MethodViewEntity>>()
    val methodList: LiveData<List<MethodViewEntity>> = _methodList

    private val _showLoadingErrorDialog = SingleLiveEvent<Unit>()
    val showLoadingErrorDialog: LiveData<Unit> = _showLoadingErrorDialog

    init {
        viewModelScope.launch(Dispatchers.Default) { setup(beerId) }
    }

    internal fun setup(beerId: Int?) = beerId?.let {
        val selectedBeerLiveData = getBeerByIdUseCase
            .execute(beerId)
            .asLiveData()

        _showLoadingErrorDialog.addSource(selectedBeerLiveData) { result ->
            result.onFailure { _showLoadingErrorDialog.postCall() }
        }

        _populateBeerDetails.addSource(selectedBeerLiveData) { result ->
            result.onSuccess { populateBeerDetails(it) }
        }

        _hopsList.addSource(selectedBeerLiveData) { result ->
            result.onSuccess { populateHopsList(it.hops) }
        }

        _maltList.addSource(selectedBeerLiveData) { result ->
            result.onSuccess { populateMaltsList(it.malts) }
        }

        _methodList.addSource(selectedBeerLiveData) { result ->
            result.onSuccess { populateMethodsList(it.methods) }
        }

    } ?: _showLoadingErrorDialog.postCall()


    internal fun populateBeerDetails(beer: Beer) {
        val viewEntity = viewEntityMapper.map(beer)
        _populateBeerDetails.postValue(viewEntity)
    }

    internal fun populateHopsList(hops: List<HopsItemRAW>) {
        val viewEntityList = hopViewEntityMapper.mapList(hops)
        _hopsList.postValue(viewEntityList)
    }

    internal fun populateMaltsList(malts: List<MaltItemRAW>) {
        val viewEntityList = maltViewEntityMapper.mapList(malts)
        _maltList.postValue(viewEntityList)
    }

    internal fun populateMethodsList(method: MethodRAW) {
        val viewEntityList = methodViewEntityMapper.map(method)
        _methodList.postValue(listOf(viewEntityList))
    }

    fun toggleHop(hopViewEntity: HopViewEntity, isChecked: Boolean) {
        val hops = _hopsList.value?.map { item ->
            item.apply { if (this == hopViewEntity) this.isChecked = isChecked }
        }

        _hopsList.value = hops
    }

    fun toggleMalt(maltViewEntity: MaltViewEntity, isChecked: Boolean) {
        val malts = _maltList.value?.map { item ->
            item.apply { if (this == maltViewEntity) this.isChecked = isChecked }
        }

        _maltList.value = malts
    }

    fun toggleMethod(methodViewEntity: MethodViewEntity, isChecked: Boolean) {
        val methods = _methodList.value?.map { item ->
            item.apply { if (this == methodViewEntity) this.isChecked = isChecked }
        }

        _methodList.value = methods
    }
}