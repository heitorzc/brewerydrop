package com.zanetti.brewery.feature.beerdetails.adapter.hop

import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.LiveData
import com.zanetti.brewery.R
import com.zanetti.brewery.core.base.adapter.ViewEntityBindingListAdapter
import com.zanetti.brewery.core.base.adapter.ViewEntityBindingViewHolder
import com.zanetti.brewery.core.base.adapter.ViewEntityDiffUtil
import com.zanetti.brewery.databinding.AdapterHopsListItemBinding
import kotlinx.android.synthetic.main.adapter_hops_list_item.view.*


class HopAdapter(lifecycleOwner: LifecycleOwner,
                 items: LiveData<List<HopViewEntity>>,
                 private val onHopeDone: (hope: HopViewEntity, isChecked: Boolean) -> Unit) :
    ViewEntityBindingListAdapter<HopViewEntity, AdapterHopsListItemBinding>(
    lifecycleOwner, items, HopItemViewEntityDiff) {

    override fun getLayoutRes(): Int {
        return R.layout.adapter_hops_list_item
    }

    override fun getViewHolder(binding: AdapterHopsListItemBinding): ViewEntityBindingViewHolder<HopViewEntity> {
        return HopViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewEntityBindingViewHolder<HopViewEntity>, position: Int) {
        super.onBindViewHolder(holder, position)
        holder.itemView.cbDone.setOnCheckedChangeListener { _, isChecked ->
            onHopeDone(getItem(position), isChecked)
        }
    }

    override fun onViewRecycled(holder: ViewEntityBindingViewHolder<HopViewEntity>) {
        super.onViewRecycled(holder)
        holder.itemView.cbDone.setOnCheckedChangeListener(null)
    }

    private companion object HopItemViewEntityDiff : ViewEntityDiffUtil<HopViewEntity>() {
        override fun areContentsTheSame(oldItem: HopViewEntity, newItem: HopViewEntity): Boolean {
            return oldItem.name == newItem.name
                    && oldItem.attribute == newItem.attribute
                    && oldItem.amount == newItem.amount
                    && oldItem.add == newItem.add
                    && oldItem.isChecked == newItem.isChecked
        }
    }
}