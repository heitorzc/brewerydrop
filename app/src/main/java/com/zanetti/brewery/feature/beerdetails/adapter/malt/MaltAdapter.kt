package com.zanetti.brewery.feature.beerdetails.adapter.malt

import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.LiveData
import com.zanetti.brewery.R
import com.zanetti.brewery.core.base.adapter.ViewEntityBindingListAdapter
import com.zanetti.brewery.core.base.adapter.ViewEntityBindingViewHolder
import com.zanetti.brewery.core.base.adapter.ViewEntityDiffUtil
import com.zanetti.brewery.databinding.AdapterMaltListItemBinding
import kotlinx.android.synthetic.main.adapter_malt_list_item.view.*


class MaltAdapter(lifecycleOwner: LifecycleOwner,
                  items: LiveData<List<MaltViewEntity>>,
                  private val onMaltDone: (hope: MaltViewEntity, isChecked: Boolean) -> Unit) :
    ViewEntityBindingListAdapter<MaltViewEntity, AdapterMaltListItemBinding>(lifecycleOwner, items, MaltViewEntityDiff) {

    override fun getLayoutRes(): Int {
        return R.layout.adapter_malt_list_item
    }

    override fun getViewHolder(binding: AdapterMaltListItemBinding): ViewEntityBindingViewHolder<MaltViewEntity> {
        return MaltViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewEntityBindingViewHolder<MaltViewEntity>, position: Int) {
        super.onBindViewHolder(holder, position)
        holder.itemView.cbDone.setOnCheckedChangeListener { _, isChecked ->
            onMaltDone(getItem(position), isChecked)
        }
    }

    override fun onViewRecycled(holder: ViewEntityBindingViewHolder<MaltViewEntity>) {
        super.onViewRecycled(holder)
        holder.itemView.cbDone.setOnCheckedChangeListener(null)
    }

    private companion object MaltViewEntityDiff: ViewEntityDiffUtil<MaltViewEntity>() {
        override fun areContentsTheSame(oldItem: MaltViewEntity, newItem: MaltViewEntity): Boolean {
            return oldItem.name == newItem.name
                    && oldItem.amount == newItem.amount
                    && oldItem.isChecked == newItem.isChecked
        }
    }
}