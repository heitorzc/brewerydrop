package com.zanetti.brewery.feature.beerlist

import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.LiveData
import com.zanetti.brewery.R
import com.zanetti.brewery.core.base.adapter.ViewEntityBindingListAdapter
import com.zanetti.brewery.core.base.adapter.ViewEntityBindingViewHolder
import com.zanetti.brewery.core.base.adapter.ViewEntityDiffUtil
import com.zanetti.brewery.databinding.AdapterBeerListItemBinding
import com.zanetti.brewery.feature.beerlist.viewentity.BeerListItemViewEntity


class BeerListAdapter(lifecycleOwner: LifecycleOwner,
                      items: LiveData<List<BeerListItemViewEntity>>,
                      private val onItemClick: (id: Int) -> Unit)
    : ViewEntityBindingListAdapter<BeerListItemViewEntity, AdapterBeerListItemBinding>(lifecycleOwner, items, BeerListItemViewEntityDiff) {

    override fun getLayoutRes(): Int {
        return R.layout.adapter_beer_list_item
    }

    override fun getViewHolder(binding: AdapterBeerListItemBinding): ViewEntityBindingViewHolder<BeerListItemViewEntity> {
        return BeerItemViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewEntityBindingViewHolder<BeerListItemViewEntity>, position: Int) {
        super.onBindViewHolder(holder, position)
        holder.itemView.setOnClickListener { onItemClick(position) }
    }

    private fun onItemClick(position: Int) {
        val itemId = getItem(position).id
        onItemClick.invoke(itemId)
    }

    private companion object BeerListItemViewEntityDiff: ViewEntityDiffUtil<BeerListItemViewEntity>() {
        override fun areContentsTheSame(oldItem: BeerListItemViewEntity, newItem: BeerListItemViewEntity): Boolean {
            return oldItem.id == newItem.id
        }
    }
}