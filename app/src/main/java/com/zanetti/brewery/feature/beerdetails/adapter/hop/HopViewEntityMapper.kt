package com.zanetti.brewery.feature.beerdetails.adapter.hop

import com.zanetti.brewery.common.core.mapper.Mapper
import com.zanetti.brewery.common.model.apiraw.AmountRAW
import com.zanetti.brewery.common.model.apiraw.HopsItemRAW

class HopViewEntityMapper: Mapper<HopsItemRAW, HopViewEntity>() {
    override fun map(item: HopsItemRAW): HopViewEntity {
        return HopViewEntity(
            name = parseName(item.name),
            attribute = parseAttribute(item.attribute),
            add = parseAdd(item.add),
            amount = parseAmount(item.amount)
        )
    }

    private fun parseName(name: String?): String {
        return name ?: "Not Available."
    }

    private fun parseAttribute(attribute: String?): String {
        return attribute ?: "Not Available."
    }

    private fun parseAdd(add: String?): String {
        return add ?: "Not Available."
    }

    private fun parseAmount(amount: AmountRAW): String {
        return "${amount.value} ${amount.unit}"
    }
}