package com.zanetti.brewery.feature.beerdetails.adapter.method

import com.zanetti.brewery.core.base.viewentity.ViewEntity

class MethodViewEntity(val mashTemp: String,
                       val fermentation: String,
                       val twist: String,
                       var isChecked: Boolean = false) : ViewEntity