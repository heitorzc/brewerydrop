package com.zanetti.brewery.feature.beerlist

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.lifecycle.observe
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.GridLayoutManager
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.zanetti.brewery.R
import kotlinx.android.synthetic.main.fragment_beer_list.*
import org.koin.android.viewmodel.ext.android.viewModel

class BeerListFragment: Fragment() {

    private val viewModel: BeerListViewModel by viewModel()

    private val adapter: BeerListAdapter by lazy {
        BeerListAdapter(
            this,
            viewModel.beerList
        ) { beerId ->
            val direction = BeerListFragmentDirections.actionBeerListFragmentToBeerDetailsFragment(beerId)
            findNavController().navigate(direction)
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_beer_list, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initView()
        observeViewModel()

    }

    private fun initView() {
        rvBeers.setHasFixedSize(true)
        rvBeers.layoutManager = GridLayoutManager(requireContext(), 1)
        rvBeers.adapter = adapter
    }

    private fun observeViewModel() {
        viewModel.showLoadingErrorDialog.observe(this) {
            MaterialAlertDialogBuilder(requireContext())
                .setTitle(R.string.error)
                .setMessage(R.string.couldnt_load_list_of_beers)
                .setPositiveButton(R.string.ok, null)
                .show()
        }

        viewModel.showNoSolutionExists.observe(this) { isVisible ->
            tvNoSolutionsExists.isVisible = isVisible
        }
    }
}