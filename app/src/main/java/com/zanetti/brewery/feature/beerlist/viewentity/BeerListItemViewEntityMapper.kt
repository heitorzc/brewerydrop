package com.zanetti.brewery.feature.beerlist.viewentity

import com.zanetti.brewery.common.core.mapper.Mapper
import com.zanetti.brewery.common.model.Beer

class BeerListItemViewEntityMapper: Mapper<Beer, BeerListItemViewEntity>() {
    override fun map(item: Beer): BeerListItemViewEntity {
        return BeerListItemViewEntity(
            id = item.id,
            name = item.name,
            abv = "${item.abv}%",
            type = item.type.name,
            imageUrl = item.imageUrl)
    }
}