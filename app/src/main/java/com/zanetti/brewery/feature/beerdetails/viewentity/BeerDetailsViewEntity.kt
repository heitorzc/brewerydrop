package com.zanetti.brewery.feature.beerdetails.viewentity

import com.zanetti.brewery.core.base.viewentity.ViewEntity

class BeerDetailsViewEntity(
    val name: String,
    val description: String,
    val abv: String,
    val imageUrl: String
) : ViewEntity {
}