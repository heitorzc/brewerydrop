package com.zanetti.brewery.feature.beerdetails.adapter.malt

import com.zanetti.brewery.core.base.viewentity.ViewEntity

class MaltViewEntity(val name: String,
                     val amount: String,
                     var isChecked: Boolean = false): ViewEntity {
}