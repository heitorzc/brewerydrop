package com.zanetti.brewery.feature.beerdetails.adapter.malt

import com.zanetti.brewery.common.core.mapper.Mapper
import com.zanetti.brewery.common.model.apiraw.AmountRAW
import com.zanetti.brewery.common.model.apiraw.MaltItemRAW

class MaltViewEntityMapper: Mapper<MaltItemRAW, MaltViewEntity>() {
    override fun map(item: MaltItemRAW): MaltViewEntity {
        return MaltViewEntity(
            name = parseName(item.name),
            amount = parseAmount(item.amount)
        )
    }

    private fun parseName(name: String?): String {
        return name ?: "Not Available."
    }

    private fun parseAmount(amount: AmountRAW): String {
        return "${amount.value} ${amount.unit}"
    }
}