package com.zanetti.brewery.feature.beerdetails.adapter.method

import com.zanetti.brewery.core.base.adapter.ViewEntityBindingViewHolder
import com.zanetti.brewery.databinding.AdapterMethodListItemBinding

class MethodViewHolder(private val binding: AdapterMethodListItemBinding): ViewEntityBindingViewHolder<MethodViewEntity>(binding.root) {
    override fun bind(item: MethodViewEntity) {
        binding.viewEntity = item
        binding.executePendingBindings()
    }
}