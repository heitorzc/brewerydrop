package com.zanetti.brewery.feature.beerdetails.adapter.hop

import com.zanetti.brewery.core.base.viewentity.ViewEntity

class HopViewEntity(val name: String,
                    val attribute: String,
                    val add: String,
                    val amount: String,
                    var isChecked: Boolean = false): ViewEntity {
}