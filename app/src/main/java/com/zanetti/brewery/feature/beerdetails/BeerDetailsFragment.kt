package com.zanetti.brewery.feature.beerdetails

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.observe
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.zanetti.brewery.R
import com.zanetti.brewery.databinding.FragmentBeerDetailsBinding
import com.zanetti.brewery.feature.beerdetails.adapter.hop.HopAdapter
import com.zanetti.brewery.feature.beerdetails.adapter.malt.MaltAdapter
import com.zanetti.brewery.feature.beerdetails.adapter.method.MethodAdapter
import kotlinx.android.synthetic.main.fragment_beer_details.*
import org.koin.android.viewmodel.ext.android.viewModel
import org.koin.core.parameter.parametersOf


class BeerDetailsFragment: Fragment() {

    private lateinit var binding: FragmentBeerDetailsBinding

    private val beerId: Int? by lazy {
        arguments?.let { BeerDetailsFragmentArgs.fromBundle(it).beerId }
    }

    private val viewModel: BeerDetailsViewModel by viewModel {
        parametersOf(beerId)
    }

    private val hopAdapter: HopAdapter by lazy {
        HopAdapter(this, viewModel.hopsList, viewModel::toggleHop)
    }

    private val maltAdapter: MaltAdapter by lazy {
        MaltAdapter(this, viewModel.maltList, viewModel::toggleMalt)
    }

    private val methodAdapter: MethodAdapter by lazy {
        MethodAdapter(this, viewModel.methodList, viewModel::toggleMethod)
    }


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_beer_details, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initView()
        observeViewModel()
    }

    private fun initView() {
        val hopsLayoutManager = LinearLayoutManager(requireContext(), RecyclerView.VERTICAL, false)
        rvHops.setHasFixedSize(true)
        rvHops.layoutManager = hopsLayoutManager
        rvHops.adapter = hopAdapter

        val maltsLayoutManager = LinearLayoutManager(requireContext(), RecyclerView.VERTICAL, false)
        rvMalts.setHasFixedSize(true)
        rvMalts.layoutManager = maltsLayoutManager
        rvMalts.adapter = maltAdapter

        val methodLayoutManager = LinearLayoutManager(requireContext(), RecyclerView.VERTICAL, false)
        rvMethods.setHasFixedSize(true)
        rvMethods.layoutManager = methodLayoutManager
        rvMethods.adapter = methodAdapter
    }

    private fun observeViewModel() {
        viewModel.populateBeerDetails.observe(this) { viewEntity ->
            binding.viewEntity = viewEntity
        }

        viewModel.showLoadingErrorDialog.observe(this) { message ->
            MaterialAlertDialogBuilder(requireContext())
                .setTitle(R.string.error)
                .setMessage(R.string.couldnt_load_beer_details)
                .setPositiveButton(R.string.ok) { _, _ -> findNavController().navigateUp() }
                .show()
        }
    }
}