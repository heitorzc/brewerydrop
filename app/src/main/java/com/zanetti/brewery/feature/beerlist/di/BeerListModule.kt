package com.zanetti.brewery.feature.beerlist.di

import com.zanetti.brewery.common.core.di.module.DepencencyModule
import com.zanetti.brewery.feature.beerlist.BeerListViewModel
import com.zanetti.brewery.feature.beerlist.viewentity.BeerListItemViewEntityMapper
import org.koin.android.viewmodel.dsl.viewModel
import org.koin.dsl.module


object BeerListModule: DepencencyModule {
    override fun get() = module {
        factory { BeerListItemViewEntityMapper() }

        viewModel { BeerListViewModel(get(), get(), get()) }
    }
}