package com.zanetti.brewery.feature.beerdetails.adapter.method

import com.zanetti.brewery.common.core.mapper.Mapper
import com.zanetti.brewery.common.model.apiraw.FermentationRAW
import com.zanetti.brewery.common.model.apiraw.MashTempItemRAW
import com.zanetti.brewery.common.model.apiraw.MethodRAW

class MethodViewEntityMapper: Mapper<MethodRAW, MethodViewEntity>() {
    override fun map(item: MethodRAW): MethodViewEntity {
        return MethodViewEntity(
            mashTemp = parseMashTemp(item.mashTemp),
            fermentation = parseFermentation(item.fermentation),
            twist = parseTwist(item.twist)
        )
    }

    private fun parseTwist(twist: String?): String {
        return twist ?: "Not Available"
    }

    private fun parseMashTemp(mashTempList: List<MashTempItemRAW>): String {
        val stringBuilder = StringBuilder()
        mashTempList.forEach { mashTemp ->
            stringBuilder
                .append("${mashTemp.temp.value}° ${mashTemp.temp.unit} for ${mashTemp.duration} minutes")
                .append("\n")
        }

        return stringBuilder.toString()
    }

    private fun parseFermentation(fermentationRAW: FermentationRAW?): String {
        return fermentationRAW?.let {
            "${fermentationRAW.temp.value}° ${fermentationRAW.temp.unit}"

        } ?: "Not Available"
    }
}