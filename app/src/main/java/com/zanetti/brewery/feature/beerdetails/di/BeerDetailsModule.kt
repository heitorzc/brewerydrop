package com.zanetti.brewery.feature.beerdetails.di

import com.zanetti.brewery.common.core.di.module.DepencencyModule
import com.zanetti.brewery.feature.beerdetails.BeerDetailsViewModel
import com.zanetti.brewery.feature.beerdetails.adapter.hop.HopViewEntityMapper
import com.zanetti.brewery.feature.beerdetails.adapter.malt.MaltViewEntityMapper
import com.zanetti.brewery.feature.beerdetails.adapter.method.MethodViewEntityMapper
import com.zanetti.brewery.feature.beerdetails.viewentity.BeerDetailsViewEntityMapper
import org.koin.android.viewmodel.dsl.viewModel
import org.koin.dsl.module

object BeerDetailsModule: DepencencyModule {
    override fun get() = module {
        factory { BeerDetailsViewEntityMapper() }
        factory { HopViewEntityMapper() }
        factory { MaltViewEntityMapper() }
        factory { MethodViewEntityMapper() }

        viewModel { (beerId: Int) -> BeerDetailsViewModel(get(), beerId, get(), get(), get(), get(), get()) }
    }
}