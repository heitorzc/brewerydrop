package com.zanetti.brewery.feature.beerdetails.adapter.method

import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.LiveData
import com.zanetti.brewery.R
import com.zanetti.brewery.core.base.adapter.ViewEntityBindingListAdapter
import com.zanetti.brewery.core.base.adapter.ViewEntityBindingViewHolder
import com.zanetti.brewery.core.base.adapter.ViewEntityDiffUtil
import com.zanetti.brewery.databinding.AdapterMethodListItemBinding
import kotlinx.android.synthetic.main.adapter_method_list_item.view.*


class MethodAdapter(lifecycleOwner: LifecycleOwner,
                    items: LiveData<List<MethodViewEntity>>,
                    private val onMethodDone: (hope: MethodViewEntity, isChecked: Boolean) -> Unit)
    : ViewEntityBindingListAdapter<MethodViewEntity, AdapterMethodListItemBinding>(lifecycleOwner, items, MethodViewEntityDiff) {

    override fun getLayoutRes(): Int {
        return R.layout.adapter_method_list_item
    }

    override fun getViewHolder(binding: AdapterMethodListItemBinding): ViewEntityBindingViewHolder<MethodViewEntity> {
        return MethodViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewEntityBindingViewHolder<MethodViewEntity>, position: Int) {
        super.onBindViewHolder(holder, position)
        holder.itemView.cbDone.setOnCheckedChangeListener { _, isChecked ->
            onMethodDone(getItem(position), isChecked)
        }
    }

    override fun onViewRecycled(holder: ViewEntityBindingViewHolder<MethodViewEntity>) {
        super.onViewRecycled(holder)
        holder.itemView.cbDone.setOnCheckedChangeListener(null)
    }

    private companion object MethodViewEntityDiff: ViewEntityDiffUtil<MethodViewEntity>() {
        override fun areContentsTheSame(oldItem: MethodViewEntity, newItem: MethodViewEntity): Boolean {
            return oldItem.twist == newItem.twist
        }
    }
}