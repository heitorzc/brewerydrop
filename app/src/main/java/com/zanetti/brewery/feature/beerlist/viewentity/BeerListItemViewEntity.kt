package com.zanetti.brewery.feature.beerlist.viewentity

import com.zanetti.brewery.core.base.viewentity.ViewEntity

data class BeerListItemViewEntity(val id: Int,
                                  val name: String,
                                  val abv: String,
                                  val type: String,
                                  val imageUrl: String,
                                  var toggleChecked: Boolean = false): ViewEntity