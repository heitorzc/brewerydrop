package com.zanetti.brewery.feature.beerdetails.viewentity

import com.zanetti.brewery.common.core.mapper.Mapper
import com.zanetti.brewery.common.model.Beer

class BeerDetailsViewEntityMapper: Mapper<Beer, BeerDetailsViewEntity>() {
    override fun map(item: Beer): BeerDetailsViewEntity {
        return BeerDetailsViewEntity(
            name = item.name,
            description = item.description,
            abv = "${item.abv}%",
            imageUrl = item.imageUrl)
    }
}