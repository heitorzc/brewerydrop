package com.zanetti.brewery.feature.beerlist

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.asLiveData
import com.zanetti.brewery.common.core.onFailure
import com.zanetti.brewery.common.core.onSuccess
import com.zanetti.brewery.core.base.SingleLiveEvent
import com.zanetti.brewery.domain.usecase.GetBeersUseCase
import com.zanetti.brewery.feature.beerlist.viewentity.BeerListItemViewEntity
import com.zanetti.brewery.feature.beerlist.viewentity.BeerListItemViewEntityMapper

class BeerListViewModel(application: Application,
                        private val mapper: BeerListItemViewEntityMapper,
                        getBeersUseCase: GetBeersUseCase): AndroidViewModel(application) {

    private val _beerList = MediatorLiveData<List<BeerListItemViewEntity>>()
    val beerList: LiveData<List<BeerListItemViewEntity>> = _beerList

    private val _showLoadingErrorDialog = SingleLiveEvent<Unit>()
    val showLoadingErrorDialog: LiveData<Unit> = _showLoadingErrorDialog

    private val _showNoSolutionExists = MediatorLiveData<Boolean>()
    val showNoSolutionExists: LiveData<Boolean> = _showNoSolutionExists

    init {
        val getBeersResult = getBeersUseCase.execute(Unit).asLiveData()

        _showLoadingErrorDialog.addSource(getBeersResult) { result ->
            result.onFailure { _showLoadingErrorDialog.postCall() }
        }

        _showNoSolutionExists.addSource(getBeersResult) { result ->
            result.onSuccess { _showNoSolutionExists.postValue(it.isEmpty()) }

        }

        _beerList.addSource(getBeersResult) { result ->
            result.onSuccess { _beerList.value =  mapper.mapList(it) }
        }
    }

}