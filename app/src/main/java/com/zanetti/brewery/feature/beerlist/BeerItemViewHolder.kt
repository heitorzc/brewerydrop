package com.zanetti.brewery.feature.beerlist

import com.zanetti.brewery.core.base.adapter.ViewEntityBindingViewHolder
import com.zanetti.brewery.databinding.AdapterBeerListItemBinding
import com.zanetti.brewery.feature.beerlist.viewentity.BeerListItemViewEntity

class BeerItemViewHolder(private val binding: AdapterBeerListItemBinding) :
    ViewEntityBindingViewHolder<BeerListItemViewEntity>(binding.root) {
    override fun bind(item: BeerListItemViewEntity) {
        binding.viewEntity = item
        binding.executePendingBindings()
    }
}