package com.zanetti.brewery.feature.beerdetails.adapter.malt

import com.zanetti.brewery.core.base.adapter.ViewEntityBindingViewHolder
import com.zanetti.brewery.databinding.AdapterMaltListItemBinding

class MaltViewHolder(private val binding: AdapterMaltListItemBinding): ViewEntityBindingViewHolder<MaltViewEntity>(binding.root) {
    override fun bind(item: MaltViewEntity) {
        binding.viewEntity = item
        binding.executePendingBindings()
    }
}