package com.zanetti.brewery.core.databinding

import android.widget.CheckBox
import android.widget.ImageView
import androidx.databinding.BindingAdapter
import coil.api.load
import com.zanetti.brewery.R

@BindingAdapter("loadFromUrl")
fun setImage(imageView: ImageView, imageUrl: String?) {
    imageView.load(imageUrl) {
        crossfade(true)
        placeholder(R.drawable.ic_launcher_background)
    }
}

@BindingAdapter("checkedState")
fun setState(checkBox: CheckBox, isChecked: Boolean?) {
    checkBox.isChecked = isChecked == true
}