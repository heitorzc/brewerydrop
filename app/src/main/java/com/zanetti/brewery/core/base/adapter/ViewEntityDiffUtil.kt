package com.zanetti.brewery.core.base.adapter

import androidx.recyclerview.widget.DiffUtil
import com.zanetti.brewery.core.base.viewentity.ViewEntity

abstract class ViewEntityDiffUtil<T: ViewEntity>: DiffUtil.ItemCallback<T>() {
    override fun areItemsTheSame(oldItem: T, newItem: T): Boolean {
        return oldItem == newItem
    }

    abstract override fun areContentsTheSame(oldItem: T, newItem: T): Boolean
}