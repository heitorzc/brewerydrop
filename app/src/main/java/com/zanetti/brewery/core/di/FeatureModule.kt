package com.zanetti.brewery.core.di

import com.zanetti.brewery.common.core.di.module.DepencencyModule
import com.zanetti.brewery.feature.beerdetails.di.BeerDetailsModule
import com.zanetti.brewery.feature.beerlist.di.BeerListModule
import org.koin.core.context.loadKoinModules
import org.koin.dsl.module

object FeatureModule: DepencencyModule {
    override fun get() = module {
        loadKoinModules(
            listOf(
                BeerListModule.get(),
                BeerDetailsModule.get()
            )
        )
    }
}