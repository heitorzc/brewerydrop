package com.zanetti.brewery.core.base.adapter

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.zanetti.brewery.core.base.viewentity.ViewEntity

abstract class ViewEntityBindingViewHolder<E: ViewEntity>(root: View): RecyclerView.ViewHolder(root) {
    abstract fun bind(item: E)
}