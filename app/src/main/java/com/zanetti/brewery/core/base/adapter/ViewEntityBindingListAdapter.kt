package com.zanetti.brewery.core.base.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.annotation.LayoutRes
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import com.zanetti.brewery.core.base.viewentity.ViewEntity

abstract class ViewEntityBindingListAdapter<E : ViewEntity, B : ViewDataBinding>(
    private val lifecycleOwner: LifecycleOwner,
    itemsLiveData: LiveData<List<E>>,
    diffUtilItemCallback: DiffUtil.ItemCallback<E>
) : ListAdapter<E, ViewEntityBindingViewHolder<E>>(diffUtilItemCallback) {

    init {
        itemsLiveData.observe(lifecycleOwner, Observer {
            submitList(it)
        })
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewEntityBindingViewHolder<E> {
        val printerItemListBinding: B = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            getLayoutRes(),
            parent,
            false
        )

        printerItemListBinding.lifecycleOwner = lifecycleOwner
        return getViewHolder(printerItemListBinding)
    }

    override fun onBindViewHolder(holder: ViewEntityBindingViewHolder<E>, position: Int) {
        val item = getItem(position)
        holder.bind(item)
    }

    @LayoutRes
    abstract fun getLayoutRes(): Int

    abstract fun getViewHolder(binding: B): ViewEntityBindingViewHolder<E>
}