package com.zanetti.brewery.core.base.viewentity

/**
 * Empty interface for design enforcement.
 */
interface ViewEntity