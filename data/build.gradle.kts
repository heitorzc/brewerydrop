plugins {
    id(Config.Plugins.androidLibrary)
    kotlin(Config.Plugins.kotlinAndroid)
}

android {
    compileSdkVersion(Config.Android.compileSdkVersion)
    buildToolsVersion(Config.Android.buildToolsVersion)

    defaultConfig {
        minSdkVersion(Config.Android.minSdkVersion)
        targetSdkVersion(Config.Android.targetSdkVersion)
        versionCode = Config.Android.versionCode
        versionName = Config.Android.versionName

        testInstrumentationRunner = Config.Android.testInstrumentationRunner
    }

    buildTypes {
        getByName("release") {
            isMinifyEnabled = true
            proguardFiles(getDefaultProguardFile("proguard-android-optimize.txt"), "proguard-rules.pro")
        }
    }

    testOptions {
        unitTests.apply {
            isIncludeAndroidResources = true
            isReturnDefaultValues = true
        }
    }
}


dependencies {

    // Kotlin Libraries
    implementation(project(":common"))

    testImplementation(Config.TestLibs.androidxTestCore)
    testImplementation(Config.TestLibs.archCoreTest)
    testImplementation(Config.TestLibs.coroutinesTest)
    testImplementation(Config.TestLibs.koinTest)
    testImplementation(Config.TestLibs.junitTest)
    testImplementation(Config.TestLibs.mockK)
    testImplementation(Config.TestLibs.roboletric)
}