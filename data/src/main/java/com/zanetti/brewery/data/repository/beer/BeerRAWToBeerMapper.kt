package com.zanetti.brewery.data.repository.beer

import com.zanetti.brewery.common.core.mapper.JoinerMapper
import com.zanetti.brewery.common.model.Beer
import com.zanetti.brewery.common.model.BeerBatchItem
import com.zanetti.brewery.common.model.BeerType
import com.zanetti.brewery.common.model.apiraw.BeerRAW

class BeerRAWToBeerMapper: JoinerMapper<BeerRAW, BeerBatchItem, Beer>() {
    override fun map(item: BeerRAW): Beer {
        return Beer(
            id = item.id,
            name = item.name ?: "Not Available",
            description = item.description ?: "Not Available",
            imageUrl = item.imageUrl ?: "",
            abv = item.abv.toString(),
            type = BeerType.BARREL_AGED,
            hops = item.ingredients.hops,
            malts = item.ingredients.malt,
            methods = item.method)
    }

    override fun mapListJoin(list1: List<BeerRAW>, list2: List<BeerBatchItem>): List<Beer> {
        return list1.map { beerRaw ->
            val batchItem = list2.find { it.beerId == beerRaw.id }
                ?: throw NullPointerException("Tried to get BeerType but BatchItem was null.")

            Beer(
                id = beerRaw.id,
                name = beerRaw.name ?: "Not Available",
                description = beerRaw.description ?: "Not Available",
                imageUrl = beerRaw.imageUrl ?: "",
                abv = beerRaw.abv.toString(),
                type = batchItem.beerType,
                hops = beerRaw.ingredients.hops,
                malts = beerRaw.ingredients.malt,
                methods = beerRaw.method)
        }
    }
}