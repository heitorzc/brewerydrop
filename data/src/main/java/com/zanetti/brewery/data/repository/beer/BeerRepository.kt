package com.zanetti.brewery.data.repository.beer

import com.zanetti.brewery.common.core.Result
import com.zanetti.brewery.common.model.Beer
import com.zanetti.brewery.data.local.BeerBatch
import com.zanetti.brewery.data.network.BreweryApi
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow

interface BeerRepository {
    fun getBeers(): Flow<Result<List<Beer>, Unit>>
    fun getBeerById(id: Int): Flow<Result<Beer, Unit>>
}

class DefaultBeerRepository(private val batch: BeerBatch,
                            private val api: BreweryApi,
                            private val beerRAWMapper: BeerRAWToBeerMapper): BeerRepository {

    private val cachedList = mutableListOf<Beer>()

    override fun getBeers(): Flow<Result<List<Beer>, Unit>> = flow {
        val beersFromBatchFile = batch.getFromBatchFile()

        if (beersFromBatchFile == null) {
            emit(Result.failure(Unit))
            return@flow
        }

        val stringIds = beersFromBatchFile
            .map { it.beerId }
            .joinToString("|")

        if (cachedList.isEmpty()) {
            val downloadedBeers = try {
                api.getBeers(stringIds)
            } catch (e: Exception) {
                emit(Result.failure(Unit))
                return@flow
            }

            val beersList = beerRAWMapper.mapListJoin(downloadedBeers, beersFromBatchFile)
            cachedList.addAll(beersList)
        }

        emit(Result.success(cachedList))
    }

    override fun getBeerById(id: Int) = flow {
         val result = cachedList
            .find { it.id == id }
            ?.let { Result.success<Beer, Unit>(it) }
            ?: Result.failure(Unit)

        emit(result)
    }
}