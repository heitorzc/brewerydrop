package com.zanetti.brewery.data.di

import com.zanetti.brewery.common.core.di.module.DepencencyModule
import com.zanetti.brewery.data.local.BeerBatch
import com.zanetti.brewery.data.local.BeerBatchImp
import com.zanetti.brewery.data.network.BreweryApi
import com.zanetti.brewery.data.repository.beer.BeerRAWToBeerMapper
import com.zanetti.brewery.data.repository.beer.BeerRepository
import com.zanetti.brewery.data.repository.beer.DefaultBeerRepository
import org.koin.dsl.module

object DataModule: DepencencyModule {
    override fun get() = module {
        factory<BeerBatch> { BeerBatchImp(get()) }
        factory { BreweryApi.getApi() }
        factory { BeerRAWToBeerMapper() }

        single<BeerRepository> {
            DefaultBeerRepository(
                get(),
                get(),
                get()
            )
        }
    }
}