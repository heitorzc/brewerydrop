package com.zanetti.brewery.data.local

import android.app.Application
import com.zanetti.brewery.common.model.BeerBatchItem
import com.zanetti.brewery.common.model.BeerType
import com.zanetti.brewery.data.R
import java.io.BufferedReader

class BeerBatchImp(private val application: Application): BeerBatch {

    override fun getFromBatchFile(): List<BeerBatchItem>? {
        val batchFileContent = readBatchFile(R.raw.beer_batch)
        return parseBatchFileContent(batchFileContent)
    }

    internal fun readBatchFile(raw: Int): String {
        val inputStream = application.resources.openRawResource(raw)
        val bufferedReader = BufferedReader(inputStream.reader())

        return bufferedReader.readText()
    }

    internal fun parseBatchFileContent(content: String): List<BeerBatchItem>? {
        try {

            // Split lines into a list excluding possible empty ones.
            val lines = content
                .split("\n")
                .filter { it.isNotBlank() }

            // Convert each client line from the file into
            // a list of Pair(beerId, beerType)
            val clients = lines
                .subList(1, lines.size)
                .map { parseBatchClientLine(it) }
                .toMutableList()

            // Check if the total beers in the first line of the file
            // matches with the total beers required by all clients.
            val totalBeersRequired = lines.first().toInt()
            val beersRequired = clients
                .flatten()
                .distinctBy { it.first }
                .count()

            // If total beers validation fails, we should
            // skip processing the file.
            if (totalBeersRequired != beersRequired) {
                return null
            }

            val beerTypes = mutableListOf<BeerBatchItem>()

            // Find impossible solutions by comparing all beerIds and beerTypes
            // required. If beers of same id are required to be produced in two
            // or more different types, that characterizes an impossible solution.
            val hasImpossibleCases = clients
                .asSequence()
                .filter { it.count() == 1 }
                .flatten()
                .distinctBy { it.first to it.second }
                .groupBy { it.first }
                .any { it.value.count() > 1 }

            // If an impossible solution was found, return an empty list to be
            // handled by the app.
            if (hasImpossibleCases) {
                return emptyList()
            }

            // First find all clients requiring only one type B beer.
            clients.iterator().apply {
                while (this.hasNext()) {
                    val beers = this.next()

                    // If this client requires more than one beer, we will
                    // process it later.
                    if (beers.count() > 1) {
                        continue
                    }

                    val beer = beers.first()

                    if (beer.second == "B") {
                        val beerId = beer.first

                        if (beerTypes.none { it.beerId == beerId }) {
                            beerTypes.add(BeerBatchItem(beer.first, BeerType.BARREL_AGED))
                        }

                        this.remove()
                    }
                }
            }

            // Process type C beers.
            clients.iterator().apply {
                while (this.hasNext()) {
                    val beers = this.next().toMutableList()

                    beers.iterator().apply {
                        while (this.hasNext()) {
                            val beer = this.next()
                            val beerId = beer.first
                            val beerType = getBeerTypeFromChar(beer.second)

                            // If we have already processed a beer with this id,
                            // we ignore it now and continue with the others.
                            if (beerTypes.any { it.beerId == beerId }) {
                                this.remove()
                                continue
                            }

                            // If a beer is requested to be of type B but the client also
                            // expects other beers to be of type C, we make this one type C
                            // as well.
                            if (beerType == BeerType.BARREL_AGED && beers.any { it.second == "C" }) {
                                beerTypes.add(BeerBatchItem(beerId, BeerType.CLASSIC))
                                this.remove()
                                continue
                            }

                            beerTypes.add(BeerBatchItem(beerId, beerType))
                            this.remove()
                        }
                    }

                    this.remove()
                }
            }

            return beerTypes.sortedBy { it.beerId }
        } catch (e: Exception) {
            return null
        }
    }

    internal fun parseBatchClientLine(line: String): List<Pair<Int, String>> {
        return Regex("(\\d+ \\w)")
            .findAll(line)
            .toList()
            .map { it.value }
            .map {
                it.substringBefore(" ").toInt() to it.substringAfter(" ")
            }
    }

    internal fun getBeerTypeFromChar(char: String): BeerType {
        return if (char == "B") BeerType.BARREL_AGED else BeerType.CLASSIC
    }
}