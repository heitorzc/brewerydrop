package com.zanetti.brewery.data.local

import com.zanetti.brewery.common.model.BeerBatchItem

interface BeerBatch {
    fun getFromBatchFile(): List<BeerBatchItem>?
}