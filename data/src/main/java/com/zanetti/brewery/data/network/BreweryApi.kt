package com.zanetti.brewery.data.network

import com.zanetti.brewery.common.model.apiraw.BeerRAW
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import retrofit2.http.GET
import retrofit2.http.Query


interface BreweryApi {

    @GET("beers")
    suspend fun getBeers(@Query("ids") ids: String): List<BeerRAW>

    companion object {
        private const val BASE_URL = "https://api.punkapi.com/v2/"

        fun getApi(): BreweryApi {
            return Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(MoshiConverterFactory.create())
                .build()
                .create(BreweryApi::class.java)
        }
    }
}