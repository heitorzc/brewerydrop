package com.zanetti.brewery.data.local

import android.app.Application
import androidx.test.core.app.ApplicationProvider
import com.zanetti.brewery.data.R
import org.junit.Assert.assertEquals
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.RobolectricTestRunner

@RunWith(RobolectricTestRunner::class)
class BeerBatchImpTest {

    private val application: Application = ApplicationProvider.getApplicationContext()

    @Test
    fun `parseBatchClientLine() - Process file beer_batch located in Resources RAW and result matches the content of file beer_batch_expected_result`() {

        val beerBatchImp = BeerBatchImp(application)
        val contentToProcess = beerBatchImp.readBatchFile(R.raw.beer_batch)
        val expectedResult = beerBatchImp.readBatchFile(R.raw.beer_batch_expected_result)

        val itemList = beerBatchImp.parseBatchFileContent(contentToProcess)
        val result = itemList?.joinToString(" ") { it.beerType.getShortName() }

        assertEquals(result, expectedResult)
    }
}