package com.zanetti.brewery.domain.di

import com.zanetti.brewery.common.core.di.module.DepencencyModule
import com.zanetti.brewery.data.di.DataModule
import com.zanetti.brewery.domain.usecase.GetBeerByIdUseCase
import com.zanetti.brewery.domain.usecase.GetBeersUseCase
import org.koin.core.context.loadKoinModules
import org.koin.dsl.module

object DomainModule: DepencencyModule {
   override fun get() = module {
        factory { GetBeersUseCase(get()) }
        factory { GetBeerByIdUseCase(get()) }

       loadKoinModules(DataModule.get())
    }
}