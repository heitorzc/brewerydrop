package com.zanetti.brewery.domain.usecase

import com.zanetti.brewery.common.core.Result
import com.zanetti.brewery.common.model.Beer
import com.zanetti.brewery.data.repository.beer.BeerRepository
import com.zanetti.brewery.domain.usecase.base.FlowUseCase
import kotlinx.coroutines.flow.Flow

class GetBeersUseCase(private val beerRepository: BeerRepository): FlowUseCase<Unit, Result<List<Beer>, Unit>>() {
    override fun execute(param: Unit): Flow<Result<List<Beer>, Unit>> {
        return beerRepository.getBeers()
    }
}