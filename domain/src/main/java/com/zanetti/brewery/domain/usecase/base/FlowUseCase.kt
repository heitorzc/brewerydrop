package com.zanetti.brewery.domain.usecase.base

import kotlinx.coroutines.flow.Flow

abstract class FlowUseCase<P, R> {
    abstract fun execute(param: P): Flow<R>
}