package com.zanetti.brewery.domain.usecase

import com.zanetti.brewery.common.core.Result
import com.zanetti.brewery.common.model.Beer
import com.zanetti.brewery.data.repository.beer.BeerRepository
import com.zanetti.brewery.domain.usecase.base.FlowUseCase
import kotlinx.coroutines.flow.Flow

class GetBeerByIdUseCase(private val beerRepository: BeerRepository): FlowUseCase<Int, Result<Beer, Unit>>() {
    override fun execute(param: Int): Flow<Result<Beer, Unit>> {
       return beerRepository.getBeerById(param)
    }
}