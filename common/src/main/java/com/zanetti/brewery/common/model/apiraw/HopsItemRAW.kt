package com.zanetti.brewery.common.model.apiraw

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class HopsItemRAW(

    @Json(name="add")
    val add: String?,

    @Json(name="amount")
    val amount: AmountRAW,

    @Json(name="name")
    val name: String?,

    @Json(name="attribute")
    val attribute: String?
)