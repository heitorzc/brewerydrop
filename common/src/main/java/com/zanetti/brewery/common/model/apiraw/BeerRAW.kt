package com.zanetti.brewery.common.model.apiraw

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class BeerRAW(

    @Json(name="first_brewed")
	val firstBrewed: String?,

    @Json(name="attenuation_level")
	val attenuationLevel: Double?,

    @Json(name="method")
	val method: MethodRAW,

    @Json(name="target_og")
	val targetOg: Double?,

    @Json(name="image_url")
	val imageUrl: String?,

    @Json(name="boil_volume")
	val boilVolume: BoilVolumeRAW,

    @Json(name="ebc")
	val ebc: Double?,

    @Json(name="description")
	val description: String?,

    @Json(name="target_fg")
	val targetFg: Double?,

    @Json(name="srm")
	val srm: Double?,

    @Json(name="volume")
	val volume: VolumeRAW,

    @Json(name="contributed_by")
	val contributedBy: String,

    @Json(name="abv")
	val abv: Double?,

    @Json(name="food_pairing")
	val foodPairing: List<String?>,

    @Json(name="name")
	val name: String?,

    @Json(name="ph")
	val ph: Double?,

    @Json(name="tagline")
	val tagline: String?,

    @Json(name="ingredients")
	val ingredients: IngredientsRAW,

    @Json(name="id")
	val id: Int,

    @Json(name="ibu")
	val ibu: Double?,

    @Json(name="brewers_tips")
	val brewersTips: String?
)
