package com.zanetti.brewery.common.core.mapper

abstract class JoinerMapper<T, K, R>: Mapper<T, R>() {
    abstract fun mapListJoin(list1: List<T>, list2: List<K>): List<R>
}