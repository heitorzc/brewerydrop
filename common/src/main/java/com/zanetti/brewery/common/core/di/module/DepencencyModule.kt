package com.zanetti.brewery.common.core.di.module

import org.koin.core.module.Module

interface DepencencyModule {
    fun get(): Module
}