package com.zanetti.brewery.common.model

import com.zanetti.brewery.common.model.apiraw.HopsItemRAW
import com.zanetti.brewery.common.model.apiraw.MaltItemRAW
import com.zanetti.brewery.common.model.apiraw.MethodRAW

data class Beer(val id: Int,
                val name: String,
                val description: String,
                val imageUrl: String,
                val abv: String,
                val type: BeerType,
                val hops: List<HopsItemRAW>,
                val malts: List<MaltItemRAW>,
                val methods: MethodRAW)