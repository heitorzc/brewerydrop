package com.zanetti.brewery.common.model

data class BeerBatchItem(val beerId: Int,
                         val beerType: BeerType) {

    override fun toString(): String {
        return "$beerId-> $beerType"
    }
}