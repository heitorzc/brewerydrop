package com.zanetti.brewery.common.model.apiraw

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class MashTempItemRAW(

    @Json(name="duration")
    val duration: Int?,

    @Json(name="temp")
    val temp: TempRAW
)