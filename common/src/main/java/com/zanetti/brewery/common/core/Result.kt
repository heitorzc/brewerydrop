package com.zanetti.brewery.common.core

class Result<T, K> private constructor(val value: T) {

    companion object {
        fun <T, K> success(value: T): Result<T, K> = Result(value)
        fun <T, K> failure(error: K): Result<T, K> = Result(Failure(error) as T)
    }

    fun isSuccess(): Boolean = this.value !is Failure<*>

    fun isFailure(): Boolean = this.value is Failure<*>

    fun errorOrNull(): K? = when (value) {
        is Failure<*> -> value.error as K
        else -> null
    }

    private class Failure<K>(val error: K)
}

inline fun <T, K> Result<T, K>.onFailure(action: (error: K) -> Unit): Result<T, K> {
    errorOrNull()?.let { action(it) }
    return this
}

inline fun <T, K> Result<T, K>.onSuccess(action: (value: T) -> Unit): Result<T, K> {
    if (isSuccess()) action(value)
    return this
}