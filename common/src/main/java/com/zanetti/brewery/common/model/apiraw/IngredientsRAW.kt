package com.zanetti.brewery.common.model.apiraw

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class IngredientsRAW(

    @Json(name="hops")
    val hops: List<HopsItemRAW>,

    @Json(name="yeast")
    val yeast: String?,

    @Json(name="malt")
    val malt: List<MaltItemRAW>
)