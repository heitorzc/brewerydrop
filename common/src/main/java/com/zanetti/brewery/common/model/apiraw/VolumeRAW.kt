package com.zanetti.brewery.common.model.apiraw

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class VolumeRAW(

    @Json(name="unit")
    val unit: String?,

    @Json(name="value")
    val value: Int?
)