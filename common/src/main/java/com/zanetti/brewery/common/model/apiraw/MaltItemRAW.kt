package com.zanetti.brewery.common.model.apiraw

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class MaltItemRAW(

    @Json(name="amount")
    val amount: AmountRAW,

    @Json(name="name")
    val name: String?
)