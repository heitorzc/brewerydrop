package com.zanetti.brewery.common.model.apiraw

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class FermentationRAW(

    @Json(name="temp")
    val temp: TempRAW
)