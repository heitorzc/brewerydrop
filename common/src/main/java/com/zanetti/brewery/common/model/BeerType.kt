package com.zanetti.brewery.common.model

enum class BeerType {
    CLASSIC, BARREL_AGED;

    fun getShortName(): String {
        return when (this) {
            CLASSIC -> "C"
            BARREL_AGED -> "B"
        }
    }
}