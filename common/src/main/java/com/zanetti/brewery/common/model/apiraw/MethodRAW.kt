package com.zanetti.brewery.common.model.apiraw

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class MethodRAW(

    @Json(name="mash_temp")
    val mashTemp: List<MashTempItemRAW>,

    @Json(name="fermentation")
    val fermentation: FermentationRAW,

    @Json(name="twist")
    val twist: String?
)