package com.zanetti.brewery.common.core.mapper

abstract class Mapper<T, R> {
    fun mapList(list: List<T>): List<R> {
        return list.map { map(it) }
    }

    abstract fun map(item: T): R

}