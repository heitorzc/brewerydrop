plugins {
    id(Config.Plugins.androidLibrary)
    kotlin(Config.Plugins.kotlinAndroid)
    kotlin(Config.Plugins.kotlinKapt)
}

android {
    compileSdkVersion(Config.Android.compileSdkVersion)
    buildToolsVersion(Config.Android.buildToolsVersion)

    defaultConfig {
        minSdkVersion(Config.Android.minSdkVersion)
        targetSdkVersion(Config.Android.targetSdkVersion)
        versionCode = Config.Android.versionCode
        versionName = Config.Android.versionName

        testInstrumentationRunner = Config.Android.testInstrumentationRunner
    }

    buildTypes {
        getByName("release") {
            isMinifyEnabled = true
            proguardFiles(getDefaultProguardFile("proguard-android-optimize.txt"), "proguard-rules.pro")
        }
    }
}


dependencies {

    // Kotlin Libraries
    api(Config.Libs.kotlinStd)
    api(Config.Libs.kotlinKtxCore)

    // Lifecycle
    api(Config.Libs.lifecycleExtensions)
    api(Config.Libs.liveDataKtx)

    // Coroutines
    api(Config.Libs.coroutines)
    api(Config.Libs.coroutinesAndroid)

    // Koin
    api(Config.Libs.koinAndroid)
    api(Config.Libs.koinViewModel)

    // Moshi Json Parser
    api(Config.Libs.moshi)
    api(Config.Libs.retrofitMoshAdapter)
    kapt(Config.Libs.moshiCodeGen)

    // Retrofit
    implementation(Config.Libs.retrofit)

    testImplementation(Config.TestLibs.androidxTestCore)
    testImplementation(Config.TestLibs.archCoreTest)
    testImplementation(Config.TestLibs.coroutinesTest)
    testImplementation(Config.TestLibs.koinTest)
    testImplementation(Config.TestLibs.junitTest)
    testImplementation(Config.TestLibs.mockK)
    testImplementation(Config.TestLibs.roboletric)
}