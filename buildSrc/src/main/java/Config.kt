import org.gradle.api.artifacts.dsl.RepositoryHandler
import org.gradle.kotlin.dsl.maven

object Config {
    private const val MAVEN_REPO_URL = "https://dl.bintray.com/kotlin/kotlin-eap"
    private const val JITPACK_URL = "https://jitpack.io"

    object Plugins {
        const val androidApplication = "com.android.application"
        const val androidLibrary = "com.android.library"
        const val kotlinAndroid = "android"
        const val kotlinAndroidExtensions = "android.extensions"
        const val kotlinKapt = "kapt"
        const val navigationSafeArgs = "androidx.navigation.safeargs.kotlin"
    }

    object Repositories {
        fun setupRepositories(handler: RepositoryHandler) = handler.apply {
            google()
            jcenter()
            mavenCentral()
            maven(url = MAVEN_REPO_URL)
            maven(url = JITPACK_URL)
        }
    }

    object BuildPlugins {
        const val androidGradle = "com.android.tools.build:gradle:${Version.GRADLE}"
        const val kotlinGradlePlugin = "org.jetbrains.kotlin:kotlin-gradle-plugin:${Version.KOTLIN}"
        const val navigationSafeArgsGradlePlugin = "androidx.navigation:navigation-safe-args-gradle-plugin:${Version.NAVIGATION}"
    }

    object Android {
        const val buildToolsVersion = "29.0.0"
        const val minSdkVersion = 23
        const val targetSdkVersion = 28
        const val compileSdkVersion = 28
        const val applicationId = "com.zanetti.brewery"
        const val versionCode = 1
        const val versionName = "0.1"
        const val testInstrumentationRunner = "androidx.test.runner.AndroidJUnitRunner"
    }

    object Libs {
        const val kotlinStd = "org.jetbrains.kotlin:kotlin-stdlib-jdk7:${Version.KOTLIN}"
        const val kotlinKtxCore = "androidx.core:core-ktx:${Version.ANDROIDX}"
        const val liveDataKtx = "androidx.lifecycle:lifecycle-livedata-ktx:${Version.LIVEDATA_KTX}"
        const val appcompat = "androidx.appcompat:appcompat:${Version.ANDROIDX}"
        const val material = "com.google.android.material:material:${Version.MATERIAL}"
        const val coroutines = "org.jetbrains.kotlinx:kotlinx-coroutines-core:${Version.COROUTINES}"
        const val coroutinesAndroid = "org.jetbrains.kotlinx:kotlinx-coroutines-android:${Version.COROUTINES}"
        const val lifecycleViewModel = "androidx.lifecycle:lifecycle-viewmodel-ktx:${Version.LIFECYCLE_VIEWMODEL}"
        const val navigationFragment = "androidx.navigation:navigation-fragment-ktx:${Version.NAVIGATION}"
        const val navigationUi = "androidx.navigation:navigation-ui-ktx:${Version.NAVIGATION}"
        const val lifecycleExtensions = "androidx.lifecycle:lifecycle-extensions:${Version.LIFECYCLE_VIEWMODEL}"
        const val koinAndroid = "org.koin:koin-android:${Version.KOIN}"
        const val koinViewModel = "org.koin:koin-android-viewmodel:${Version.KOIN}"
        const val moshi = "com.squareup.moshi:moshi:${Version.MOSHI}"
        const val moshiCodeGen = "com.squareup.moshi:moshi-kotlin-codegen:${Version.MOSHI}"
        const val retrofit = "com.squareup.retrofit2:retrofit:${Version.RETROFIT}"
        const val retrofitMoshAdapter = "com.squareup.retrofit2:converter-moshi:${Version.RETROFIT}"
        const val coil = "io.coil-kt:coil:${Version.COIL}"
    }

    object TestLibs {
        const val androidxTestCore = "androidx.test:core-ktx:${Version.ANDROIDX_TEST_CORE}"
        const val archCoreTest = "androidx.arch.core:core-testing:${Version.ARCH_CORE_TEST}"
        const val junitTest = "junit:junit:${Version.JUNIT_TEST}"
        const val junitAndroidTest = "androidx.test.ext:junit:${Version.JUNIT_ANDROID_TEST}"
        const val espressoCore = "androidx.test.espresso:espresso-core:${Version.ESPRESSO_CORE}"
        const val koinTest = "org.koin:koin-test:${Version.KOIN}"
        const val coroutinesTest = "org.jetbrains.kotlinx:kotlinx-coroutines-test:${Version.COROUTINES}"
        const val mockK = "io.mockk:mockk:${Version.MOCKK}"
        const val roboletric = "org.robolectric:robolectric:${Version.ROBOLETRIC}"
    }
}