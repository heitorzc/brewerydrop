# Brewery Challenge #

The app was built using Android Studio 3.6 RC3.
If required to change Gradle Version for building using older versions, you can do that by locating the `Version` file in the `buildSrc` directory and change the `GRADLE` value.

### Load beers from file ###

The logic to load beers from file is located in the class `BeerBatchImp` in the `data` module. It will read the file from the raw resource directory.
There you can find two files:

- `beer_batch.txt` which is where the data will be read from
- `beer_batch_expected_result.txt` which will be used by the unit test to verify the expected output


### Technologies ###
Some of the technologies used in this project include:

- Android Clean Architecture with MVVVM pattern
- Android Navigation Component
- Android Data Binding
- Kotlin Coroutines Flow
- Koin Dependency Injection
- Retrofit
- Moshi JSON parser
- Coil image loading

### Modules ###
The project is split into 4 modules:

- App
- Common
- Data
- Domain

The communication between data and presentation was implemented by using Flow and LiveData.